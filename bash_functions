#!/bin/bash

# Get directory of this script
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
export TRACKED_CFG=$DIR

# Update installed package list each time apm is run successfully
function apm {
	echo "$@"
	/usr/bin/apm "$@" && /usr/bin/apm list --installed --bare > $TRACKED_CFG/atom-packages.txt
}
